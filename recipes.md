# Cobra's Fiery 1 Degree Sauce Whiskey Sour

This twist on the classic whiskey sour cocktail includes Papa John's garlic dipping sauce and hot sauce for a savory and spicy flavor. Shake whiskey or rum with garlic dipping sauce, lemon juice, honey syrup, and hot sauce, then strain into a glass and garnish with a lemon wheel or garlic clove.

# Sexy Gothic Bad Boy

This dark and mysterious cocktail combines black vodka, blackberry liqueur, lime juice, simple syrup, orange bitters, and absinthe. Shake the ingredients with ice, strain into a glass filled with ice, and garnish with fresh blackberries.

# The Cobra Venom

This bold and spicy drink is made with Fireball whiskey and apple cider vinegar. Mix one shot of Fireball with one tablespoon of apple cider vinegar, then top with Mountain Dew to taste. This drink packs a punch, so be prepared!


# The Gothic King Cobra

This dark and mysterious drink combines black cherry soda with black vodka and a splash of grenadine. Fill a glass with ice, then add one shot of black vodka and top with black cherry soda. Add a splash of grenadine for a touch of sweetness, and garnish with a black cherry.


# The Cobra Kai

This drink is inspired by the popular TV series, and is made with green tea, lemonade, and sake. Mix equal parts green tea and lemonade, then add a shot of sake and stir. Serve over ice and garnish with a lemon wedge.


# The Warlord

This drink is made with Red Bull, Jägermeister, and a splash of cranberry juice. Fill a glass with ice, then add one shot of Jägermeister and a splash of cranberry juice. Top with Red Bull and stir gently.


# The Cobra's Kiss

This sweet and fruity drink combines pickle juice with strawberry soda and rum. Fill a glass with ice, then add one shot of rum and top with equal parts pickle juice and strawberry soda. Stir gently and enjoy.


# The Peanut Butter & Jelly Cobra

This creamy and sweet drink combines peanut butter whiskey, grape jelly, milk, and ice cream. Blend two scoops of vanilla ice cream with one shot of peanut butter

